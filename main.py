
import pandas as pd
import gurobipy as gp
import numpy as np
from typing import List, Dict
import altair as alt

#############################################
# Define maximum time per line
lines_Dict = {"MyBeer",
              "MyBud",
              "MyIPA"}

# List of production lines available
lines: [str] = list(lines_Dict)




#Bottle types
Bottle_type = {'A',
               'B'}

#Flavor types
Flavor = ['Honey',
          'Malt']

#Ingredients types
Ingredients = ['Sugar',
               'Salt']

###########################################################
# Equipment Process timing dictionary
Process_Dict = {
    'BottleRec': {
        'MyBeer': {'A': 0.01,
                   'B': 00},
        'MyBud': {'A': 0.01,
                  'B': 0.03 },
        'MyIPA': {'A': 0,
                  'B': 0.02 }},

    'WaterFilling': {
        'MyBeer': 0.06,
        'MyBud': 0.05,
        'MyIPA': 0.04},
    'Flavor': {
        'MyBeer': {'Honey': 0.02,
                   'Malt': 0.02},
        'MyBud': {'Honey': 0,
                  'Malt': 0.01},
        'MyIPA': {'Honey': 0.02,
                  'Malt': 0}
    },

    'Ingredients': {
        'MyBeer': {'Sugar': 0.01,
                   'Salt': 0.02},
        'MyBud': {'Sugar': 0,
                  'Salt': 0.01},
        'MyIPA': {'Sugar': 0,
                  'Salt': 0},
    },

    'QualityCheck': {
        'MyBeer': 0.1,
        'MyBud': 0.1,
        'MyIPA': 0.07},

    'Packaging': {
        'MyBeer': 0.04,
        'MyBud': 0.06,
        'MyIPA': 0.09}

}
#process names
Process: [str] = list(Process_Dict)


#Wating time tables
Wait_Dict = {

    'BottleRec' :{'WaterFilling':0.02,
                  'Ingredients':0.02,
                  'QualityCheck':0.01},

    'WaterFilling' :{'BottleRec':0.01,
                     'Ingredients':0.04,
                     'QualityCheck':0.02},

    'Ingredients':{ 'BottleRec': 0.03,
                    'WaterFilling': 0.02,
                    'QualityCheck': 0.04}
}

#wait keys
Wait: [str] = list(Wait_Dict)

###################################################
#bottles per pack
Pack = 6

#maximum modeling time
max_time=2

#time keys
max_time_key = list(map(str,range(1,max_time)))

#Quality check stop duration
QC_wait =0.6
####################################################

#creat the model
model = gp.Model("Optimize production planning")



Process_hours = model.addVars(
    Bottle_type,
    Flavor,
    Ingredients,
    lines,
    lb=0,
    ub=max_time,
    vtype=gp.GRB.CONTINUOUS,
    name="process",
)


# Variable Production
prod = model.addVars(
    Bottle_type,
    Flavor,
    Ingredients,
    lines,
    lb=0,
    vtype=gp.GRB.CONTINUOUS,
    name="Production"
)




model.addConstrs(
        (
            Process_hours[(ty,fl,ing,li)] == (
            Process_Dict['BottleRec'][li][ty] + Wait_Dict['BottleRec']['WaterFilling']+
            Process_Dict['WaterFilling'][li] +Wait_Dict['WaterFilling']['Ingredients']+
            Process_Dict['Flavor'][li][fl] +
            Process_Dict['Ingredients'][li][ing]+Wait_Dict['Ingredients']['QualityCheck']+
            Process_Dict['QualityCheck'][li] +QC_wait+
            Process_Dict['Packaging'][li])
            for li in lines
            for ty in Bottle_type
            for fl in Flavor
            for ing in Ingredients
        ),
        name="processhours",
    )


model.addConstrs(
    (
    prod[(ty,fl,ing,li)] == Process_hours[ (ty,fl,ing,li)] * Pack
        for li in lines
        for ty in Bottle_type
        for fl in Flavor
        for ing in Ingredients
        for ti in max_time_key
    ),
    name="totalhours",
)


# Set the value of cost (hours/ randeman per hour)
model.addConstrs(
    (
    (Process_hours[ (ty,fl,ing,li)] * prod[(ty,fl,ing,li)]) <= max_time
        for li in lines
        for ty in Bottle_type
        for fl in Flavor
        for ing in Ingredients
        for ti in max_time_key
    ),
    name="totalhours",
)



# Objective : minimize a function
model.ModelSense = gp.GRB.MAXIMIZE
# Function to minimize
optimization_var = gp.quicksum(
    (Process_hours[(ty,fl,ing,li)] * prod[(ty,fl,ing,li)])
    for li in lines
    for ty in Bottle_type
    for fl in Flavor
    for ing in Ingredients
    for ti in max_time_key

)
objective = 0
objective += optimization_var

# SOLVE MODEL
model.setObjective(objective)
model.optimize()

model


#if model.status == gp.OPTIMAL:
#    model.printAttr('X')


rows= lines.copy()
columns = prod.keys()
make_plan = pd.DataFrame(columns=columns, index=rows, data=0.0)


for ty, fl, ing, li in prod.keys():
    if (abs(prod[ty,fl,ing,li].x) > 1e-6):
        make_plan[ty,fl,ing,li][li] = np.round(prod[ty,fl,ing,li].x, 1)
make_plan

